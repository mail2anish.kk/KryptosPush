"use strict";
var gcm = require('node-gcm');
var apn = require('apn');
var q = require('q');
var fs = require("fs");

var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var pushConfig = require('../../config/pushconfig');


exports.createForumPost = function (req, res, next) {

    if (req.body && req.body.tenant && req.body.areaofInterest && req.body.title && req.body.body) {

        var tenant = req.body.tenant;
        var areaofInterest = req.body.areaofInterest;
        var title = req.body.title;
        var body = req.body.body;
        var picture = req.body.picture;
        var userEmail = req.body.userEmail;
        var userImage = req.body.userImage;
        var postedBy = req.body.postedBy;


        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_FEED";
                var dateTimeStamp = new Date();
                var feedData = {
                    'userEmail': userEmail,
                    'userImage': userImage,
                    'postedBy': postedBy,
                    'title': title,
                    'areaofInterest': areaofInterest,
                    'body': body,
                    'picture': (picture == undefined ? '' : picture),
                    'likes': '',
                    'cmnts': '',
                    'views': '',
                    'date': dateTimeStamp,
                    tenant: tenant
                };

                db.collection(tableName).insertOne(feedData, function (err, result3) {
                    if (err) {
                        res.json({
                            "error": "Something Occurred"
                        });
                    } else {

                        res.json({
                            "success": "Forum data Added"
                        });

                    }

                });
            });
        } catch (e) {
            console.log(e)
        }

    }
    else {
        res.json({
            "Error": "Parameter missing"
        });
    }
}


exports.getForumPost = function (req, res, next) {

    if (req.body && req.body.tenant && req.body.areaofInterest && req.body.userEmail) {

        var tenant = req.body.tenant;
        var areaofInterest = req.body.areaofInterest;
        var userEmail = req.body.userEmail;
        var skip = req.body.skip;
        if (!skip)
            skip = 0;
        var limit = req.body.limit;
        if (!limit)
            limit = 5;

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_FEED";
                db.collection(tableName).find({areaofInterest: {$in: areaofInterest}}).sort({date:-1}).skip(skip).limit(limit).toArray(function (err, result) {
                    //console.log(result);
                    if (result.length > 0) {

                        var data = {};

                        data['last'] = result.length < limit;

                        var postIds = [];

                        for (var i = 0; i < result.length; i++) {
                            postIds.push(result[i]['_id'].toString());
                        }

                        //console.log(postIds);

                        getLikesCount(postIds, tenant, function (err, likeResult) {

                            getCommentCount(postIds, tenant, function (err, commentResult) {


                                getUserLike(postIds, tenant, userEmail, function (err, userLikes) {


                                    for (var i = 0; i < result.length; i++) {
                                        result[i]['likes'] = getCount(likeResult, result[i]['_id'].toString());
                                        result[i]['cmnts'] = getCount(commentResult, result[i]['_id'].toString());
                                        result[i]['isliked'] = isLiked(userLikes, result[i]['_id'].toString());
                                    }

                                    data['content'] = result;
                                    return res.json(data);

                                })
                            })

                        })

                    } else {
                        var data = {
                            content: [],
                            last: true
                        };

                        return res.json(data);
                    }
                });

            });
        } catch (e) {
            console.log(e)
        }

    }
    else {
        res.json({
            "Error": "Parameter missing"
        });
    }
}
function isLiked(userLikes, postId) {
    var flag = false;
    for (var i = 0; i < userLikes.length; i++) {
        if (userLikes[i]['_id'] === postId) {
            flag = true;
            break;
        }
    }
    return flag;
}

function getCount(result, postId) {
    var count = 0;
    for (var i = 0; i < result.length; i++) {
        if (result[i]['_id'] == postId) {
            count = result[i]['total'];
            break;
        }

    }
    return count;

}
function getUserLike(postIdArray, tenant, userEmail, cb) {

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMLIKES";

            db.collection(tableName).aggregate([
                {
                    "$match": {
                        "forumId": {$in: postIdArray},
                        "userEmail": userEmail
                    }
                },
                {
                    "$group": {
                        "_id": "$forumId",
                        "total": {
                            "$sum": 1
                        }
                    }
                }
            ]).toArray(function (err, result) {
                console.log(result);
                cb(null, result);
            });

        });
    } catch (e) {
        console.log(e)
    }

}

function getCommentCount(postIdArray, tenant, cb) {

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMCOMMENTS";

            db.collection(tableName).aggregate([
                {
                    "$match": {
                        "forumId": {$in: postIdArray}
                    }
                },
                {
                    "$group": {
                        "_id": "$forumId",
                        "total": {
                            "$sum": 1
                        }
                    }
                }
            ]).toArray(function (err, result) {
                console.log(result);
                cb(null, result);
            });

        });
    } catch (e) {
        console.log(e)
    }

}
function getLikesCount(postIdArray, tenant, cb) {

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMLIKES";

            db.collection(tableName).aggregate([
                {
                    "$match": {
                        "forumId": {$in: postIdArray}
                    }
                },
                {
                    "$group": {
                        "_id": "$forumId",
                        "total": {
                            "$sum": 1
                        }
                    }
                }
            ]).toArray(function (err, result) {
                console.log(result);
                cb(null, result);
            });

        });
    } catch (e) {
        console.log(e)
    }

}


function getisliked(postIdArray, tenant, cb) {

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMLIKES";

            db.collection(tableName).aggregate([
                {
                    "$match": {
                        "userEmail": {$in: userEmail}
                    }
                },
                {
                    "$group": {
                        "_id": "$forumId",
                        "total": {
                            "$sum": 1
                        }
                    }
                }
            ]).toArray(function (err, result) {
                console.log(result);
                cb(null, result);
            });

        });
    } catch (e) {
        console.log(e)
    }

}


exports.deleteForumPost = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.id) {
        var tenant = req.body.tenant;
        var id = req.body.id;

        //var tableName = "T_" + tenant + "_" + channelId + "_POSTCOMMENTS";
        var tableName = "T_" + tenant + "_FEED";
        dbUtil.getConnection(function (db) {
            db.collection(tableName).findOneAndDelete({
                "_id": ObjectId(id)
            }, function (err, result) {
                if (result.value != null) {
                    res.json({"success": "post deleted"});
                } else {
                    res.json({
                        "error": "post not found"
                    });
                }
            });
        });
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}


exports.likeForumPost = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.id && req.body.userEmail) {
        var tenant = req.body.tenant;
        var id = req.body.id;
        var userEmail = req.body.userEmail;
        var userName = req.body.userName;
        var userImage = req.body.userImage;

        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMLIKES";
            db.collection(tableName).find({
                "forumId": id,
                "userEmail": userEmail
            }).toArray(function (err, result) {
                if (result.length > 0) {
                    db.collection(tableName).findOneAndDelete({
                        "forumId": id,
                        "userEmail": userEmail
                    }, function (err, result) {
                        if (err) {
                            res.json({
                                "error": "error occurred"
                            });
                        } else {
                            res.json({
                                "success": "Unliked"
                            });
                        }
                    });
                } else {
                    db.collection(tableName).insertOne({
                        "forumId": id,
                        "userEmail": userEmail,
                        "userName": userName,
                        "userImage": userImage
                    }, function (err, result) {
                        if (err) {
                            res.json({
                                "error": "error occurred"
                            });
                        } else {
                            res.json({
                                "success": "liked"
                            });
                        }
                    });
                }
            });
        });
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}


exports.getLikes = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.id) {
        var tenant = req.body.tenant;
        var id = req.body.id;

        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMLIKES";
            db.collection(tableName).find({
                "forumId": id
            }).toArray(function (err, result) {
                if (result.length > 0) {
                    res.json({
                        "likes": result
                    });
                } else {
                    res.json({
                        "error": "no likes"
                    });
                }
            })

        });
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}


exports.commentForumPost = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.id && req.body.userEmail) {
        var tenant = req.body.tenant;
        var id = req.body.id;
        var userEmail = req.body.userEmail;
        var comment = req.body.comment;
        var date = new Date();
        var userImage = req.body.userImage;
        var userName = req.body.userName;


        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMCOMMENTS";
            db.collection(tableName).insertOne({
                "forumId": id,
                "userEmail": userEmail,
                "comment": comment,
                "date": date,
                "userImage": userImage,
                "userName": userName

            }, function (err, result) {
                if (err) {
                    res.json({
                        "error": "error occurred"
                    });
                } else {
                    res.json({
                        "success": "Commented"
                    });
                }
            });

        });
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}


exports.getComments = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.id) {
        var tenant = req.body.tenant;
        var id = req.body.id;

        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_FORUMCOMMENTS";
            db.collection(tableName).find({
                "forumId": id
            }).toArray(function (err, result) {
                if (result.length > 0) {
                    res.json({
                        "Comments": result
                    });
                } else {
                    res.json({
                        "error": "no comments"
                    });
                }
            })

        });
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}


exports.editForumPost = function (req, res, next) {

    if (req.body && req.body.tenant && req.body.areaofInterest && req.body.title && req.body.body) {

        var tenant = req.body.tenant;
        var id = req.body.id;
        var userEmail = req.body.userEmail;
        var userImage = req.body.userImage;
        var postedBy = req.body.postedBy;
        var title = req.body.title;
        var areaofInterest = req.body.areaofInterest;
        var body = req.body.body;
        var picture = req.body.picture;
        var likes = req.body.likes;
        var cmnts = req.body.cmnts;
        var views = req.body.views;
        var date = req.body.date;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_FEED";
                var dateTimeStamp = new Date();
                var feedData = {
                    'tenant': tenant,
                    'id': id,
                    'userEmail': userEmail,
                    'userImage': userImage,
                    'postedBy': postedBy,
                    'title': title,
                    'picture': (picture == undefined ? '' : picture),
                    'areaofInterest': areaofInterest,
                    'body': body,
                    'likes': likes,
                    'cmnts': cmnts,
                    'views': views,
                    'date': dateTimeStamp.getDate()
                };
                db.collection(tableName).find({_id: ObjectId(id)}).toArray(function (err, result) {

                    if (result.length > 0) {
                        db.collection(tableName).updateOne({_id: ObjectId(id)}, feedData, function (err, result3) {
                            if (err) {
                                res.json({
                                    "error": "Something Occurred"
                                });
                            } else {

                                res.json({
                                    "success": "Editing of Forum Post Successful"
                                });

                            }

                        });

                    } else {
                        return res.json({success: false, message: 'Post not found.'})
                    }

                })
            });
        } catch (e) {
            console.log(e)
        }

    }
    else {
        res.json({
            "Error": "Parameter missing"
        });
    }
}





