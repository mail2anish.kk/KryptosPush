var cron = require('node-schedule');
var dbUtil = require("../../config/dbUtil");
var request = require('request')

var notificationScheduler = function(){
    console.log('calling scheduler ....');

    cron.scheduleJob('0 */10 * * * *', function(){
        console.log('Scheduler runs every 10 minute...');
        dbUtil.getConnection(function (db) {
            var webScrappingLinks = db.collection("announcements_links");
            webScrappingLinks.find({status: 'active'}).toArray(function (err, result) {
                var count = 0;
                var total_count = result.length-1;
                webScrape(result,total_count, count, function(){
                    console.log('notification checked for all the tenants...  ')
                })
            });
        });
    });

 function webScrape(linkList, total_count, count,cb) {
        //count = 2
        console.log('current web scrapping : ', linkList[count].tenant)  
        var tenant = linkList[count].tenant
        var urlOptions = {
            method: 'GET',
            url: linkList[count].link,
        };

        request(urlOptions, function (error, response, body) {
            // console.log('error:', error); 
            // console.log('statusCode:', response && response.statusCode); 
            // console.log('body:', body); 
           
            if(JSON.stringify(body).includes('504 Gateway Time-out')){
                console.log('response: ', JSON.stringify(body));
                webScrape(linkList, total_count, count, cb)
            }else{
                var postData = {
                    "links": JSON.parse(body).links,
                    "tenant": tenant
                  }

                  var urlOptions = {
                    method: 'POST',
                    //url: "http://localhost:8082//kryptosds/user/getNewsList",
                    url: "https://push.kryptosmobile.com/kryptosds/user/getNewsList",
                    json: postData
                };
                
                request(urlOptions, function (error, response, body) {
                    // console.log('error:', error);
                    // console.log('statusCode:', response && response.statusCode); 
                    console.log('body:', body); 
                    console.log('web scrape count: ', count); 
                    if(count < total_count){
                        count = count + 1; 
                        webScrape(linkList, total_count, count, cb)
                    }else{
                        cb();
                    }
                });
            }
        });
    }
}




module.exports = notificationScheduler;